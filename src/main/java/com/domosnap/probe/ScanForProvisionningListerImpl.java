package com.domosnap.probe;

import java.io.UnsupportedEncodingException;

/*
 * #%L
 * domosnap-probe
 * %%
 * Copyright (C) 2020 - 2023 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.MessageFormat;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.supervisor.OnScanDeviceListener;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;

public class ScanForProvisionningListerImpl implements OnScanDeviceListener {

	private final String url;
	private final String host;
	private final boolean ssl;
	private final int port;
	
	private final String gateway;
	private final Log log = new Log(this.getClass().getSimpleName());
	private final WebClient client;
	
	public ScanForProvisionningListerImpl(Vertx vertx, String gateway, String smartdeviceHost, int port, String smartdeviceServiceUrl, boolean ssl,  WebClient client) {
		this.gateway = gateway;
		this.host = smartdeviceHost;
		this.url = smartdeviceServiceUrl;
		this.ssl = ssl;
		this.port = port;
		this.client = client;
	}
	
	@Override
	public void foundController(Device device) {
		try {
			log.fine(Session.DEVICE, MessageFormat.format("Device who[{0}], Where[{1}] found!", device.getType(), device.getWhere()));
			
			SmartDeviceServiceAdapter smartdeviceService = new SmartDeviceServiceAdapter(device, ssl, host, port, url, client);
			
			// Update the device
			smartdeviceService.updateDevice();

			// Add a listener to update measure when their value change
			device.addControllerChangeListener((Device dev, What oldStatus, What newStatus) -> {
				// Update the measure!
				if (oldStatus.getValue() == null) {
					try {
						smartdeviceService.updateMeasure(newStatus);
					} catch (UnsupportedEncodingException e) {
						log.severe(Session.OTHER, e.getMessage());
					}
				}
			});
		} catch (Exception e) {
			log.severe(Session.OTHER, e.getMessage());
		}
	}

	@Override
	public void progess(int percent) {
		log.fine(Session.DEVICE, MessageFormat.format("Protocole {0} scan progress [{1}%].", gateway, percent));
	}

	@Override
	public void scanFinished() {
		log.fine(Session.DEVICE, MessageFormat.format("Protocole {0} scan finish.", gateway));
	}
}
