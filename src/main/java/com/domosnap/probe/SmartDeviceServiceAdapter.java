package com.domosnap.probe;

/*
 * #%L
 * domosnap-probe
 * %%
 * Copyright (C) 2020 - 2023 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateMappingRegistry;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.what.What;

import io.vertx.core.AsyncResult;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

public class SmartDeviceServiceAdapter {

	private String url;
	private String host;
	private boolean ssl;
	private int port;
	private Device device;
	
	private WebClient client;
	
	private Log log = new Log(this.getClass().getSimpleName());
	
	public SmartDeviceServiceAdapter(Device device, boolean ssl, String host, int port, String url, WebClient client) {
		this.device = device;
		this.ssl = ssl;
		this.host = host;
		this.port = port;
		this.url = url;
		this.client = client;
	}
	
	public void updateDevice() throws UnsupportedEncodingException {
		// Get Device
		HttpRequest<Buffer> request =  client.get(port, host, url + "?uri=" + URLEncoder.encode(device.getWhere().getURI().toString(), "UTF-8"));
		request.ssl(ssl);
		request.send(this::onGetDevice);
	}
	
	private void onGetDevice(AsyncResult<HttpResponse<Buffer>> response) {
		if (response.succeeded() && response.result().statusCode() == 200) {
			if (response.result().bodyAsJsonArray().size() > 0) {
				if (log.debugLevel) {
					log.finest(Session.OTHER, "Controller [" + device.getWhere().getPath() + "] already exist.");
				}
				return;
			} 
			// If not existing, we create the device
			HttpRequest<Buffer> request =  client.post(port, host, url);
			request.ssl(ssl);
			request.putHeader("Content-Type", "application/json");
			request.sendJson(toJson(device), this::onPostDevice);
		} else {
			if (log.warningLevel) {
				try {
					log.warning(Session.OTHER, "Error to provisionning device [" + device.getWhere() + "] to GET [" + (ssl ? "https://" : "http://") + host + ":" + port + url + "?uri=" + URLEncoder.encode(device.getWhere().getURI().toString(), "UTF-8") + "]. Response: " + response.result());
				} catch (UnsupportedEncodingException e) {
					log.severe(Session.OTHER, "Impossible errors occurs?:" + e.getMessage());
				}
			}
		}
	}
	
	private void onPostDevice(AsyncResult<HttpResponse<Buffer>> response) {
			if (response.succeeded() && response.result().statusCode() == 201) {
				if (log.debugLevel) {
					log.finest(Session.OTHER, "Device [" + device.getWhere() + "] has been provisioned.");
				}
			} else {
				log.warning(Session.OTHER, "Error to provisionning device [" + device.getWhere() + "] to POST [" + (ssl ? "https://" : "http://") + host + ":" + port + url + "]. Response: " + response.result());
			}
	}

	@SuppressWarnings("unchecked")
	private JsonObject toJson(Device device) {
		String smartdevice = "{\"uri\":\"".concat(String.valueOf(device.getWhere())).concat("\",\"type\":\"").concat(device.getType()).concat("\",\"active\":true, \"measures\": [");
		
		Map<String, State<?>> stateMap = device.getStateMap();
		
		boolean first = true;
		for (StateName stateKey : device.getStateList()) {
			State<?> state = stateMap.get(stateKey.name());
			if (state != null) {
				if (first) {
					first = false;
				} else {
					smartdevice = smartdevice.concat(",");
				}
				smartdevice = smartdevice.concat("{\"key\":\"" + stateKey + "\",\"type\":\"" + 	StateMappingRegistry.getMapping((Class<? extends State<?>>) state.getClass())  + "\", \"description\": \"Measure automatically Created with controller.\"}");
			}
		}
		
		smartdevice = smartdevice.concat("]}"); // TODO apply a parse on the getWhere!!!
		return new JsonObject(smartdevice);
	}
	
	
	public void updateMeasure(What newStatus) throws UnsupportedEncodingException {
		HttpRequest<Buffer> request =  client.get(port, host, url + "?uri=" + URLEncoder.encode(device.getWhere().getURI().toString(), "UTF-8"));
		request.ssl(ssl);

		MeasureUpde mu = new MeasureUpde(newStatus);
		
		request.send(mu::onGetDeviceToUpdateMeasure);

	}
	
	private class MeasureUpde {
		
		private What newStatus;
		
		public MeasureUpde(What newStatus) {
			this.newStatus = newStatus;
		}
		
		private void onGetDeviceToUpdateMeasure(AsyncResult<HttpResponse<Buffer>> response) {
			if (response.succeeded() && response.result().statusCode() == 200  && response.result().bodyAsJsonArray().size() > 0) {
				JsonObject smartdevice = response.result().bodyAsJsonArray().getJsonObject(0);
				JsonArray measures = smartdevice.getJsonArray("measures");
				boolean create = true;
				for (int i = 0; i < measures.size() ; i++) {
					JsonObject measure = measures.getJsonObject(i);
					if (newStatus.getName().equals(measure.getString("key"))) {
						create = false;
						break;
					}
				}
				
				if (create) {
					String uid = smartdevice.getString("id");
					HttpRequest<Buffer> requestPOST =  client.post(port, host, url + "/" + uid + "/measures");
					requestPOST.ssl(ssl);
					requestPOST.putHeader("Content-Type", "application/json");
					requestPOST.sendJson(toJson(newStatus), this::onPostMeasure);
				}
			}
		}
		
		private void onPostMeasure(AsyncResult<HttpResponse<Buffer>> response) {
			if (response.succeeded() && response.result().statusCode() == 200) {
				if (log.debugLevel) {
					log.finest(Session.OTHER, "Controller [" + device.getWhere() + "] has been updated.");
				}
			} else {
				log.warning(Session.OTHER, "Error to update controller [" + device.getWhere() + "]");
			}
		}

		private JsonObject toJson(What newStatus) {
			@SuppressWarnings("unchecked")
			String measure = "{\"key\":\"" + newStatus.getName() + "\",\"type\":\"" + StateMappingRegistry.getMapping((Class<? extends State<?>>) newStatus.getValue().getClass()) + "\"," 
					+ "\"description\": \"Measure automatically Created.\"}";
			
			return new JsonObject(measure);
		}
		
	}
}
