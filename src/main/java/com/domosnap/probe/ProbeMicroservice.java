package com.domosnap.probe;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import com.domosnap.core.adapter.i2c.I2CAdapter;
import com.domosnap.core.adapter.onewire.OneWireAdapter;

/*
 * #%L
 * domosnap-probe
 * %%
 * Copyright (C) 2020 - 2023 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.core.consumer.kafka.EventToKafkaConsumer;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.event.EventFactory;
import com.domosnap.engine.event.EventToFileConsumer;
import com.domosnap.engine.gateway.GatewayBuilder;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.gateway.UnsupportedScheme;
import com.domosnap.engine.supervisor.DeviceDiscovery;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.auth.oauth2.OAuth2FlowType;
import io.vertx.ext.auth.oauth2.OAuth2Options;
import io.vertx.ext.auth.oauth2.Oauth2Credentials;
import io.vertx.ext.auth.oauth2.providers.KeycloakAuth;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.HealthChecks;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.OAuth2WebClient;
import io.vertx.ext.web.client.OAuth2WebClientOptions;
import io.vertx.ext.web.client.WebClient;

public class ProbeMicroservice extends AbstractVerticle {
	
	private static final String FILE_PATH_KEY = "file.path";
	private static final String LOG_ENABLE = "log.enable";
	private static final String SCS_GATEWAY_URL = "scs.gateway.url";
	private static final String SMARTDEVICE_ENABLED = "provisionning";
	private static final String SMARTDEVICE_URI = "smartdevice.service.uri"; 
	private static final String SMARTDEVICE_HOST = "smartdevice.service.host"; 
	private static final String SMARTDEVICE_HOST_SSL = "smartdevice.service.ssl";
	private static final String SMARTDEVICE_PORT = "smartdevice.service.port";
	private static final String CLIENTID = "security.clientId";
	private static final String CLIENTSECRET = "security.clientSecret";
	private static final String SITE = "security.site";
	private static final String TENANT = "security.tenant";
	private static final String KAFKA_BOOTSTRAP = "kafka.bootstrap_server";
	private static final String KAFKA_TOPIC = "kafkac.topic";

	
	
	
	private final Log log = new Log(ProbeMicroservice.class.getSimpleName());
	
	@Override
	public void start() {

		Log.getConfig().put("all", config().getValue(LOG_ENABLE, "true"));
		
		String path = config().getString(FILE_PATH_KEY, "/opt/probe/logs");
		JsonArray gatewayList = config().getJsonArray(SCS_GATEWAY_URL, new JsonArray(List.of("scs://12345@192.168.1.35:20000"))); // "scs://12345@localhost:1234" , "scs://12345@192.168.1.35:20000"
		String url = config().getString(SMARTDEVICE_URI, "/devices");
		String host = config().getString(SMARTDEVICE_HOST, "localhost");
		int port = config().getInteger(SMARTDEVICE_PORT, 8080);
		boolean ssl = config().getBoolean(SMARTDEVICE_HOST_SSL, false);
		boolean provisionning = config().getBoolean(SMARTDEVICE_ENABLED, false);

		final String clientId = config().getString(CLIENTID,"probe");
		final String clientSecret = config().getString(CLIENTSECRET, "9Nq5UTuEuqWhhjXLfYf50kNE7jntlD7k");
		final String site  = config().getString(SITE, "https://keycloak.arnauddegiuli.info/realms/domosnap");
		final String tenant  = config().getString(TENANT, "domosnap");
		final String kafka_bootstrap = config().getString(KAFKA_BOOTSTRAP, "");
		final String kafka_topic = config().getString(KAFKA_TOPIC, "domosnap-event-topic");

		

		
		EventFactory.addConsumer(new EventToFileConsumer(path));
		if (!"".equals(kafka_bootstrap)) {
			EventFactory.addConsumer(new EventToKafkaConsumer(kafka_bootstrap, kafka_topic));
		}
		DeviceDiscovery dd = new DeviceDiscovery();
		
		vertx.executeBlocking(future -> {
			for(int i = 0; i < gatewayList.size(); i++ ) {
				
					try {
						GatewayBuilder.registerProtocole(OneWireAdapter.SCHEME, OneWireAdapter.class);
						GatewayBuilder.registerProtocole(I2CAdapter.SCHEME, I2CAdapter.class);
						String gateway = gatewayList.getString(i);

						if (provisionning) {
							KeycloakAuth.discover(vertx,  new OAuth2Options()
									.setClientId(clientId)
									.setClientSecret(clientSecret)
									.setSite(site)
									.setTenant(tenant))
									.onSuccess( oauth -> {
										OAuth2WebClient client = OAuth2WebClient.create(
										        WebClient.create(vertx),
										        oauth,
												new OAuth2WebClientOptions().setRenewTokenOnForbidden(true) // Renew token when expired.
												)
										      .withCredentials(
										    	 new Oauth2Credentials().setFlow(OAuth2FlowType.CLIENT).setUsername(clientId).setPassword(clientSecret))
										        ;
										try {

											dd.scan(new URI(gateway), new ScanForProvisionningListerImpl(vertx, gateway, host, port, url, ssl, client));
										} catch (IllegalArgumentException | UnsupportedAdapter | URISyntaxException e) {
											log.severe(Session.GATEWAY, e.getMessage());
										}
									})
									.onFailure( error -> {
										error.printStackTrace();
										try {
											dd.scan(new URI(gateway), new ScanListenerImpl(gateway));
										} catch (IllegalArgumentException | UnsupportedAdapter | URISyntaxException e) {
											log.severe(Session.GATEWAY, e.getMessage());
										}
									});
						} else {
							try {
								dd.scan(new URI(gateway), new ScanListenerImpl(gateway));
							} catch (IllegalArgumentException | UnsupportedAdapter | URISyntaxException e) {
								log.severe(Session.GATEWAY, e.getMessage());
							}
						}

					} catch (UnsupportedScheme e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
			}
		}, null);
		
		
		// Create healthcheck
		HealthChecks hc = HealthChecks.create(vertx);

		// Register with a timeout. The check fails if it does not complete in time.
		// The timeout is given in ms.
		hc.register("ownCheckHealth", 200, future -> {
			
//							if (dd.isConnected()) {
//								connectionTry = 0;
				future.complete(Status.OK());
//							} else {
//								if (connectionTry < 10) {
//									connectionTry++;
//									dd.connect();
//									future.complete(Status.OK());
//								} else {
//									future.complete(Status.KO());
//								}
//							}
		});
		
		// Register the health check handler
		Router router = Router.router(vertx);
		router.get("/health*").handler(HealthCheckHandler.createWithHealthChecks(hc));

		vertx.createHttpServer().requestHandler(router).listen(8089, ar ->
			System.out.println("Health started on port " + ar.result().actualPort())
		);

		System.out.println("#    _____                                                    _____           _          ");
		System.out.println("#   |  __ \\                                                  |  __ \\         | |         ");
		System.out.println("#   | |  | | ___  _ __ ___   ___  ___ _ __   __ _ _ __ ______| |__) | __ ___ | |__   ___ ");
		System.out.println("#   | |  | |/ _ \\| '_ ` _ \\ / _ \\/ __| '_ \\ / _` | '_ \\______|  ___/ '__/ _ \\| '_ \\ / _ \\");
		System.out.println("#   | |__| | (_) | | | | | | (_) \\__ \\ | | | (_| | |_) |     | |   | | | (_) | |_) |  __/");
		System.out.println("#   |_____/ \\___/|_| |_| |_|\\___/|___/_| |_|\\__,_| .__/      |_|   |_|  \\___/|_.__/ \\___|");
		System.out.println("#                                                | |                                     ");
		System.out.println("#                                                |_|");
		System.out.println("Probe Service Started");
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		System.out.println("Prove Service Stopped");
	}

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(ProbeMicroservice.class.getName());
	}
}
