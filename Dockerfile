FROM eclipse-temurin:19-jre-jammy

WORKDIR /opt/probe
COPY ./target/domosnap-probe-*.jar ./domosnap-probe.jar
COPY ./src/main/configuration/default-configuration.json ./conf/config.json

ENTRYPOINT ["java", "-jar", "domosnap-probe.jar", "-Duser.dir=/tmp", "-conf ./conf/config.json"]