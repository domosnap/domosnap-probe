# Domosnap-probe-project

The project does 3 things:

*  Scan devices for the list of gateway
*  If provisionning activated, call the domosnap-smartdevice service to declare a device
*  send event from all detected device to a file.

Lunch with java -jar -conf configuration.json

    {
    "file.path": "/opt/probe/logs",
    "log.enable": true,
    "scs.gateway.url":["scs://xxxxx@192.168.1.35:20000"],
    "provisionning":true,
    "smartdevice.service.host":localhost,
    "smartdevice.service.port":80,
    "smartdevice.service.uri":"/devices",
    "smartdevice.service.ssl":false,
    "security.clientId": "xxx",
    "security.clientSecret":"xxx",
    "security.site":"https://keycloak.info/realms/tenant",
    "security.tenant":"tenant",
    }



| Parameter                  |Value Type| Default                        |Example|
|----------------------------|--|--------------------------------|--|
| "file.path"                |String| "/opt/probe/logs"              |"/opt/probe/logs"|
| "log.enable"               |boolean| false                          |true|
| "scs.gateway.url"          |String| none                           |"scs://12345@192.168.1.126:20000"|
| "provisionning"            |boolean| false                          |true|
| "smartdevice.service.host" |String| "localhost"                    |"smartdevice.arnauddegiuli.info"|
| "smartdevice.service.port" |int| 8080                           |443|
| "smartdevice.service.uri"  |String| "/devices"                     |"/devices"|
| "smartdevice.service.ssl"  |boolean| false                          |true|
| "security.clientId"        |String| null                           |"probe"|
| "security.clientSecret"    |String| null                           |"xxxxx"|
| "security.site"            |String| null                           |"https://keycloak.info/realms/tenant"|
| "security.tenant"          |String| null                           |"tenant"|
| "kafka.bootstrap_server"   |String| "" (event consumer desactived) |"192.168.1.60:31478"|
| "kafka.topic"              |String| "domosnap-event-topic"         |"domosnap-event-topic"|


## Prerequis

*  maven 3
*  OpenJDK 13
 
## Run

### From eclipse

Lunch the class: 
   
    io.vertx.core.Launcher

with parameter:

    run com.domosnap.probe.ProbeMicroservice -conf $home/git/domosnap/domosnap-probe/src/main/configuration/default-configuration.json

### On your laptop
	mvn clean package
	cd target
	java -jar probe-project-0.x.x-SNAPSHOT.jar -conf ../src/main/configuration/default-configuration.json
    

### On raspberry cluster

    mvn clean package
    sudo docker login registry.gitlab.com
    sudo docker build -f Dockerfile_arm64v8 . -t registry.gitlab.com/domosnap/domosnap-probe:arm64v8-latest
    sudo docker push registry.gitlab.com/domosnap/domosnap-probe:arm64v8-latest
    kubectl apply -f probe-project-manual.yaml
    kubectl create configmap probe-project --from-file=config.json=./src/main/configuration/default-configuration.json -n domosnap-probe
    

### On pipeline

To be done

### Remark
For new project don't forget to register the token:
    - Create key in gitlab (Project->Settings->Repository->Deploy Tokens with a username and password)
    - On k8s cluster add the key:

    kubectl create secret docker-registry domosnap-secret --docker-server=registry.gitlab.com --docker-username='gitlab-deploy-token' --docker-password='generated-token'

[More info](https://kubernetes.io/fr/docs/tasks/configure-pod-container/pull-image-private-registry/ "More info")


Don't forget to use imagePullSecret in pod definition: 
    
    imagePullSecrets:
        - name: domosnap-secret

